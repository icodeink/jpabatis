package com.yinsin.jpabatis.type;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Array;
import java.sql.Date;
import java.sql.Ref;
import java.sql.Struct;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

public enum TypeConvert {
	
	ARRAY(Array.class, Types.ARRAY),
	BIT(Boolean.class, Types.BIT), 
	TINYINT(Byte.class, Types.TINYINT), 
	SMALLINT(Short.class, Types.SMALLINT), 
	INTEGER(Integer.class, Types.INTEGER), 
	BIGINT(BigInteger.class, Types.BIGINT), 
	LONG(Long.class, Types.BIGINT), 
	FLOAT(Double.class, Types.FLOAT), 
	REAL(Float.class, Types.REAL), 
	DOUBLE(Double.class, Types.DOUBLE), 
	NUMERIC(BigDecimal.class, Types.NUMERIC), 
	DECIMAL(BigDecimal.class, Types.DECIMAL), 
	CHAR(String.class, Types.CHAR), 
	VARCHAR(String.class, Types.VARCHAR), 
	LONGVARCHAR(String.class, Types.LONGVARCHAR), 
	DATE(Date.class, Types.DATE), 
	TIME(Time.class, Types.TIME), 
	TIMESTAMP(Timestamp.class, Types.TIMESTAMP), 
	BINARY((new byte[0]).getClass(), Types.BINARY), 
	VARBINARY((new byte[0]).getClass(), Types.VARBINARY), 
	LONGVARBINARY((new byte[0]).getClass(), Types.LONGVARBINARY), 
	OTHER(Object.class, Types.OTHER), 
	BLOB((new byte[0]).getClass(), Types.BLOB), 
	CLOB(String.class, Types.CLOB), 
	BOOLEAN(Boolean.class, Types.BOOLEAN), 
	STRUCT(Struct.class, Types.STRUCT), 
	JAVA_OBJECT(Object.class, Types.JAVA_OBJECT), 
	REF(Ref.class, Types.REF);
	
	public final Class<?> TYPE_CLASS;
	public final Integer TYPE_CODE;
	private static Map<Class<?>, Integer> codeLookup = new HashMap<>();
	
	static {
		for (TypeConvert type : TypeConvert.values()) {
			codeLookup.put(type.TYPE_CLASS, type.TYPE_CODE);
		}
	}

	TypeConvert(Class<?> classz, int code) {
		this.TYPE_CLASS = classz;
		this.TYPE_CODE = code;
	}
	
	public static Integer forClass(Class<?> classz) {
		return codeLookup.get(classz);
	}
}
