package com.yinsin.jpabatis.util;

import java.util.regex.Pattern;

public interface Constants {

    /**
     * 过滤所有以&lt;开头, 以&gt;结尾的标签(过滤html标签)的正则表达式
     */
    public final static Pattern REG_HTML = Pattern.compile("<([^>]*)>");

    /**
     * 数字正则表达式
     */
    public static final Pattern REG_INTEGER = Pattern.compile("(\\+|\\-)?\\d+");

    /**
     * 小写字符正则表达式
     */
    public static final Pattern REG_IS_LOWCASE = Pattern.compile("[a-z]+");

    /**
     * 大写字符正则表达式
     */
    public static final Pattern REG_IS_UPPERCASE = Pattern.compile("[A-Z]+");

    /**
     * 十六进制字符
     */
    public static final char[] CHAR_ARRAY = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    /**
     * 手机号正则表达式
     */
    public static final Pattern REG_MOBILE = Pattern.compile("^(13[0-9]|14[5|7]|15[0-3|5-9]|18[0|2|3|5-9])\\d{8}$");

    /**
     * 邮件地址正则表达式
     */
    public static final Pattern REG_MAIL = Pattern.compile("^\\b[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b$");

    /**
     * 字符集编码UTF-8
     */
    public static final String CHARSET_UTF8 = "utf-8";

    /**
     * 字符集编码GBK
     */
    public static final String CHARSET_GBK = "gbk";

    /**
     * 字符集编码gb2312
     */
    public static final String CHARSET_GB2312 = "gb2312";

    /**
     * 字符集编码ISO-8859-1
     */
    public static final String CHARSET_ISO88591 = "iso-8859-1";

    /**
     * 空字符串
     */
    public static final String CHAR_EMPTY = "";

    /**
     * 日期格式化字符串常量，间隔符号为横杠"-"<br>
     * yyyy-MM-dd
     */
    public static final String FORMAT_DATE_H = "yyyy-MM-dd";

    /**
     * 日期格式化字符串常量，间隔符号为斜杠"/"<br>
     * yyyy/MM/dd
     */
    public static final String FORMAT_DATE_S = "yyyy/MM/dd";

    /**
     * 日期格式化字符串常量，间隔符号为中文"年、月、日"<br>
     * yyyy年MM月dd日
     */
    public static final String FORMAT_DATE_C = "yyyy年MM月dd日";

    /**
     * 日期时间格式化字符串常量，间隔符号为横杠"-"<br>
     * yyyy-MM-dd HH:mm:ss
     */
    public static final String FORMAT_DATE_TIME_H = "yyyy-MM-dd HH:mm:ss";

    /**
     * 日期时间格式化字符串常量，间隔符号为斜杠"/"<br>
     * yyyy/MM/dd HH:mm:ss
     */
    public static final String FORMAT_DATE_TIME_S = "yyyy/MM/dd HH:mm:ss";

    /**
     * 日期时间格式化字符串常量，间隔符号为中文"年、月、日"<br>
     * yyyy年MM月dd日 HH时mm分ss秒
     */
    public static final String FORMAT_DATE_TIME_C = "yyyy年MM月dd日 HH时mm分ss秒";

    /**
     * 时间格式化字符串常量，间隔符号为冒号":"<br>
     * HH:mm:ss
     */
    public static final String FORMAT_TIME = "HH:mm:ss";
    
    /**
     * ISO时间格式化字符串常量<br>
     * yyyy-MM-dd'T'HH:mm:ss
     */
    public static final String ISO_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     * 时间格式化字符串常量，间隔符号为中文"时、分、秒"<br>
     * HH时mm分ss秒
     */
    public static final String FORMAT_TIME_C = "HH时mm分ss秒";

    public final static String PRIVATE = "private ";
    public final static String PUBLIC = "public ";
    public final static String OBJECT = "java.lang.Object";
    public final static String STRING = "java.lang.String";
    public final static String INTEGER = "java.lang.Integer";
    public final static String LONG = "java.lang.Long";
    public final static String FLOAT = "java.lang.Float";
    public final static String DOUBLE = "java.lang.Double";
    public final static String DATE = "java.util.Date";
    public final static String TIMESTAMP = "java.sql.Timestamp";
    public final static String LBYTE = "[B";
    public final static String GET = "get";
    public final static String SET = "set";
    public final static String ENTER = "\r\n";
    public final static String TAB1 = "    ";
    public final static String TAB2 = "        ";
    public final static String TAB3 = "            ";
    public final static String TAB4 = "                    ";
    public final static String TAB5 = "                        ";

}
