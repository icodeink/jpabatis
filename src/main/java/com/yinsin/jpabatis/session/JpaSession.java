package com.yinsin.jpabatis.session;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.yinsin.jpabatis.config.Configuration;
import com.yinsin.jpabatis.executor.Executor;

public class JpaSession {

	protected Configuration configuration;
	protected Executor executor;
	protected SqlSession session;

	@SuppressWarnings("unused")
	private JpaSession() {
	}

	public JpaSession(Configuration configuration) {
		executor = configuration.newExecutor();
		session = new DefaultSqlSession(configuration, executor);
	}
	
	public <T> T selectOne(String id) {
		return session.selectOne(id);
	}

	public <T> T selectOne(String id, Object param) {
		return session.selectOne(id, param);
	}

	public <T> List<T> selectList(String id, Object param) {
		return session.selectList(id, param);
	}
	
	public <T> List<T> selectList(String id) {
		return session.selectList(id);
	}

	public <T> Page<T> selectList(String id, Object param, Pageable pageabel) {
		RowBounds rowBounds = new RowBounds(pageabel.getOffset(), pageabel.getPageSize());
		Page<T> page = session.selectList(id, param, rowBounds);
		return page;
	}
	
	public <T> Page<T> selectList(String id, Pageable pageabel) {
		RowBounds rowBounds = new RowBounds(pageabel.getOffset(), pageabel.getPageSize());
		Page<T> page = session.selectList(id, rowBounds);
		return page;
	}
}
