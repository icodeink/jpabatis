package com.yinsin.jpabatis.exceptions;

public class JpaBatisException extends RuntimeException {
	private static final long serialVersionUID = 3880206998166270511L;

	public JpaBatisException() {
		super();
	}

	public JpaBatisException(String message) {
		super(message);
	}

	public JpaBatisException(String message, Throwable cause) {
		super(message, cause);
	}

	public JpaBatisException(Throwable cause) {
		super(cause);
	}
}
