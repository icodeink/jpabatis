# jpabatis
## 一、介绍
基于mybatis封装jpa-batis插件，方便使用jpa的项目能像使用mybatis一样能非常方便的处理复杂的查询sql语句。
主要是为了复杂的查询而生，所以我把insert、update、delete的支持都干掉了，只支持select。

因是基于mybatis改装的，所以mapper.xml里的写法跟mybatis一模一样，底层与数据库交互是基于jpa的EntityManager类来实现的。

各位码友觉得有用的给个star，也可以fork去完善完善，觉得无用的勿喷，谢谢。

## 二、软件架构
软件架构说明
基于：
1、[mybatis-3](https://github.com/mybatis/mybatis-3)

2、Spring Data JPA

## 三、安装教程
当前版本是基于springboot1.5.10.RELEASE版本封装，如果想使用高版本springboot，自己修改parent的版本即可
```
<parent>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-parent</artifactId>
	<version>1.5.10.RELEASE</version>
</parent>
```
在您的工程里引入jpa-batis.jar包
```
<dependency>
    <groupId>com.yinsin</groupId>
    <artifactId>jpabatis</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>
```

## 四、使用说明

### 1、在springboot配置文件中增加Mapper.xml文件的路径配置
```java
batis:
    mapper: mappers # 例如，src/main/resources/mappers/XyzMapper.xml
```
或者
```java
batis.mapper=mappers # 例如，src/main/resources/mappers/XyzMapper.xml
```

### 2、编写Mapper.xml：
```java
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.yinsin.lifechess.domain.game.CllUserGameInfo">
    <resultMap id="BaseResultMap" type="java.util.Map">
        <result column="row_id" jdbcType="BIGINT" property="rowId" />
        <result column="account" jdbcType="VARCHAR" property="account" />
        <result column="name" jdbcType="VARCHAR" property="name" />
        <result column="mobile" jdbcType="VARCHAR" property="mobile" />
        <result column="email" jdbcType="VARCHAR" property="email" />
        <result column="headimage" jdbcType="VARCHAR" property="headimage" />
        <result column="status" jdbcType="INTEGER" property="status" />
        <result column="game_integral" jdbcType="INTEGER" property="gameIntegral" />
        <result column="game_level" jdbcType="INTEGER" property="gameLevel" />

    </resultMap>
    
    <select id="loadCChessRankingList" parameterType="String" resultMap="BaseResultMap">
    	select 
		  ui.row_id, ui.account, ui.name, ui.mobile,
		  ui.email, ui.headimage, ui.status,
		  ugi.game_integral, ugi.game_level
		from
		  cll_user_info ui 
		  left join cll_user_game_info ugi 
		    on ui.row_id = ugi.user_id 
		where ugi.game_no = #{gameNo,jdbcType=VARCHAR}
		order by ugi.game_integral desc,
		  ugi.game_level desc,
		  ui.lastlogindate desc
    </select>
</mapper>
```

### 3、需要使用jpabatis的类中注入JpaSession类：
```java
@JpaMapper
private JpaSession session;
```
  
### 4、执行调用：
#### 4.1 查询单条记录
```java
Map<String, Object> param = new HashMap<>();
param.put("gameNo", "xx");
// mapperid -> mapper的namespace + <select>的id
// 例如：com.yinsin.lifechess.domain.game.CllUserGameInfo.loadCChessRankingList
Map<String, Object> result = session.selectOne("mapperid", param);
// TODO ...
```

#### 4.2 查询多条记录
```java
Map<String, Object> param = new HashMap<>();
param.put("gameNo", "xx");
// mapperid -> mapper的namespace + <select>的id
// 例如：com.yinsin.lifechess.domain.game.CllUserGameInfo.loadCChessRankingList
List<Map<String, Object>> dataList = session.selectList("mapperid", param);
// TODO ...
```

#### 4.3 查询多条记录并分页
```java
// 分页查询和非分页查询，只有一个Pageable参数的区别，mapper中的sql不需要写limit
Map<String, Object> param = new HashMap<>();
param.put("gameNo", "xx");
Pageable pageable = new PageRequest(0, 10); //1.x版本
//Pageable pageable = Pageable.of(0, 10); //2.x版本
// mapperid -> mapper的namespace + <select>的id
// 例如：com.yinsin.lifechess.domain.game.CllUserGameInfo.loadCChessRankingList
Page<Map<String, Object>> dataList = session.selectList("mapperid", param, pageable);
// TODO ...
```